"""
Module for defining Django models for the Website app.

This module defines several Django models that represent entities
used in the Website apps

"""


from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator


# Create your models here.
class Accomodation(models.Model):
    name = models.CharField("accomodation name", max_length=150)

    def __str__(self):
        return str(self.name)


class Resa(models.Model):
    date_sent = models.DateTimeField(blank=True, null=True)
    check_in = models.DateField()
    check_out = models.DateField(blank=True, null=True)
    service = models.ForeignKey(Accomodation, on_delete=models.CASCADE)
    pax = models.IntegerField(
        default=1,
        editable=True,
        validators=[MinValueValidator(1), MaxValueValidator(200)],
    )
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    phone = models.CharField(max_length=15)
    email_address = models.EmailField()
    description = models.TextField(blank=True)
    status = models.CharField(blank=True, null=True, max_length=10)

    def __str__(self):
        return str(self.first_name + " " + self.last_name)


class MessageContact(models.Model):
    date_sent = models.DateTimeField(blank=True, null=True)
    message_name = models.CharField(max_length=200)
    message_email = models.EmailField()
    message_content = models.TextField()
    status = models.CharField(blank=True, null=True, max_length=10)

    def __str__(self):
        return str(self.message_name)


# Create your models here.


class SlideHome(models.Model):
    image_number = models.CharField(max_length=10)
    slide_image1 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image2 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image3 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image4 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image5 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image6 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image7 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image8 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image9 = models.ImageField(null=True, blank=True, upload_to="images/slides/")
    slide_image10 = models.ImageField(null=True, blank=True, upload_to="images/slides/")

    def __str__(self):
        return str(self.image_number)


class ComponentHome(models.Model):
    title = models.CharField(max_length=500)
    translated_title = models.CharField(max_length=500, null=True, blank=True)
    text_home1 = models.TextField()
    translated_text_home1 = models.TextField(null=True, blank=True)
    text_home2 = models.TextField(
        null=True,
        blank=True,
    )
    translated_text_home2 = models.TextField(null=True, blank=True)
    text_home3 = models.TextField(
        null=True,
        blank=True,
    )
    translated_text_home3 = models.TextField(null=True, blank=True)
    logo = models.ImageField(null=True, blank=True, upload_to="images/")
    component_image = models.ImageField(null=True, blank=True, upload_to="images/")

    def __str__(self):
        return str(self.title)


class OfferHome(models.Model):
    offer_title = models.CharField(max_length=100)
    translated_offer_title = models.CharField(max_length=100, null=True, blank=True)
    offer_details = models.TextField()
    translated_offer_details = models.TextField(null=True, blank=True)
    offer_image = models.ImageField(null=True, blank=True, upload_to="images/")

    def __str__(self):
        return str(self.offer_title)


class OfferHomeDetails(models.Model):
    title = models.CharField(max_length=100)
    translated_title = models.CharField(max_length=100)
    footer_text1 = models.CharField(max_length=400)
    translated_footer_text1 = models.CharField(max_length=400, null=True, blank=True)
    footer_text2 = models.CharField(max_length=400)
    translated_footer_text2 = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return str(self.title)


class ParagrapheTodoHome(models.Model):
    todo_title = models.CharField(max_length=400)
    translated_todo_title = models.CharField(max_length=400, null=True, blank=True)
    todo = models.TextField()
    translated_todo = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.todo_title)


class ItemTodoHome(models.Model):
    item_image = models.ImageField(null=False, blank=False, upload_to="images/")
    item_image2 = models.ImageField(null=True, blank=True, upload_to="images/")
    item_details = models.TextField()
    translated_item_details = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.item_details)


class Navbar(models.Model):
    accueil = models.CharField(max_length=20)
    translated_accueil = models.CharField(max_length=20)
    resto = models.CharField(max_length=20)
    translated_resto = models.CharField(max_length=20)
    chambre = models.CharField(max_length=20)
    translated_chambre = models.CharField(max_length=20)
    activite = models.CharField(max_length=20)
    translated_activite = models.CharField(max_length=20)
    gallerie = models.CharField(max_length=20)
    translated_gallerie = models.CharField(max_length=20)
    contact = models.CharField(max_length=20)
    translated_contact = models.CharField(max_length=20)
    lang = models.CharField(max_length=20)
    translated_lang = models.CharField(max_length=20)
    book = models.CharField(max_length=20)
    translated_book = models.CharField(max_length=20)

    def __str__(self):
        return str(self.accueil)


class Footer(models.Model):
    big_text = models.TextField()
    translated_big_text = models.TextField(null=True, blank=True)
    big_text2 = models.TextField(null=True, blank=True)
    translated_big_text2 = models.TextField(null=True, blank=True)
    logo = models.ImageField(null=True, blank=True)
    small_text = models.TextField()
    translated_small_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.big_text)


class Contacts(models.Model):
    email_address = models.EmailField()
    addresse = models.CharField(max_length=200)
    phone_number1 = models.CharField(max_length=20)
    phone_number2 = models.CharField(max_length=20)
    phone_number3 = models.CharField(max_length=20)
    facebook_link = models.CharField(max_length=200)
    instagram_link = models.CharField(max_length=200)

    def __str__(self):
        return str(self.email_address)


class ParagrapheAnimeResto(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    big_text = models.TextField()
    translated_big_text = models.TextField(null=True, blank=True)
    small_text = models.TextField()
    translated_small_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.title)


class Card1Resto(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    details = models.TextField(null=True, blank=True)
    translated_details = models.TextField(null=True, blank=True)
    paragraphe_title = models.CharField(max_length=200, null=True, blank=True)
    translated_paragraphe_title = models.CharField(
        max_length=200, null=True, blank=True
    )
    paragraphe_text = models.TextField()
    translated_paragraphe_text = models.TextField(null=True, blank=True)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/resto1/")

    def __str__(self):
        return str(self.title)


class Card2Resto(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    details = models.TextField(null=True, blank=True)
    translated_details = models.TextField(null=True, blank=True)
    paragraphe_title = models.CharField(max_length=200, null=True, blank=True)
    translated_paragraphe_title = models.CharField(
        max_length=200, null=True, blank=True
    )
    paragraphe_text = models.TextField()
    translated_paragraphe_text = models.TextField(null=True, blank=True)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/resto2/")

    def __str__(self):
        return str(self.title)


class Card3Resto(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    details = models.TextField(null=True, blank=True)
    translated_details = models.TextField(null=True, blank=True)
    paragraphe_title = models.CharField(max_length=200, null=True, blank=True)
    translated_paragraphe_title = models.CharField(
        max_length=200, null=True, blank=True
    )
    paragraphe_text = models.TextField()
    translated_paragraphe_text = models.TextField(null=True, blank=True)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/resto3/")

    def __str__(self):
        return str(self.title)


class ParagrapheFootResto(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    text = models.TextField()
    translated_text = models.TextField(null=True, blank=True)
    boutton = models.CharField(max_length=200, null=True, blank=True)
    translated_boutton = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.text)


class CardBedrooms(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    details = models.TextField(null=True, blank=True)
    translated_details = models.TextField(null=True, blank=True)
    price = models.CharField(max_length=50)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/bedrooms/")

    def __str__(self):
        return str(self.title)


class ParagrapheBedrooms(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    title2 = models.CharField(max_length=200, null=True, blank=True)
    translated_title2 = models.CharField(max_length=200, null=True, blank=True)
    text = models.TextField()
    translated_text = models.TextField(null=True, blank=True)
    boutton = models.CharField(max_length=200, null=True, blank=True)
    translated_boutton = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.title)


class ParagrapheActivities(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    text = models.TextField()
    translated_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.title)


class CardActivities(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/activities/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/activities/")

    def __str__(self):
        return str(self.title)


class GalleryGrid(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    image1 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image2 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image3 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image4 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image5 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image6 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image7 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image8 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image9 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")
    image10 = models.ImageField(null=True, blank=True, upload_to="images/gallery/")

    def __str__(self):
        return str(self.title)


class LocalisationVohitsoa(models.Model):
    title = models.CharField(max_length=200)
    translated_title = models.CharField(max_length=200, null=True, blank=True)
    url = models.CharField(max_length=1000)
    title2 = models.CharField(max_length=200, null=True, blank=True)
    translated_title2 = models.CharField(max_length=200, null=True, blank=True)
    placeh1 = models.CharField(max_length=200, null=True, blank=True)
    translated_placeh1 = models.CharField(max_length=200, null=True, blank=True)
    placeh2 = models.CharField(max_length=200, null=True, blank=True)
    translated_placeh2 = models.CharField(max_length=200, null=True, blank=True)
    placeh3 = models.CharField(max_length=200, null=True, blank=True)
    translated_placeh3 = models.CharField(max_length=200, null=True, blank=True)
    boutton = models.CharField(max_length=200, null=True, blank=True)
    translated_boutton = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.title)
