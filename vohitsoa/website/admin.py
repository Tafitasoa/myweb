from django.contrib import admin
from .models import Accomodation
from .models import Resa
from .models import MessageContact

from .models import SlideHome
from .models import ComponentHome
from .models import OfferHome
from .models import ParagrapheTodoHome
from .models import ItemTodoHome
from .models import Navbar
from .models import Footer
from .models import Contacts
from .models import ParagrapheAnimeResto
from .models import ParagrapheFootResto
from .models import Card1Resto
from .models import Card2Resto
from .models import Card3Resto
from .models import CardBedrooms
from .models import ParagrapheBedrooms
from .models import CardActivities
from .models import ParagrapheActivities
from .models import GalleryGrid
from .models import LocalisationVohitsoa
from .models import OfferHomeDetails

# Register your models here.
# Register your models here.
admin.site.register(Accomodation)
admin.site.register(MessageContact)

admin.site.register(SlideHome)
admin.site.register(ComponentHome)
admin.site.register(OfferHome)
admin.site.register(Navbar)
admin.site.register(Footer)
admin.site.register(Contacts)
admin.site.register(ParagrapheTodoHome)
admin.site.register(ItemTodoHome)
admin.site.register(ParagrapheFootResto)
admin.site.register(ParagrapheAnimeResto)
admin.site.register(Card1Resto)
admin.site.register(Card2Resto)
admin.site.register(Card3Resto)
admin.site.register(CardBedrooms)
admin.site.register(ParagrapheBedrooms)
admin.site.register(CardActivities)
admin.site.register(ParagrapheActivities)
admin.site.register(GalleryGrid)
admin.site.register(LocalisationVohitsoa)
admin.site.register(OfferHomeDetails)


@admin.register(Resa)
class ResaAdmin(admin.ModelAdmin):
    fields = (
        ("check_in", "check_out"),
        "service",
        "pax",
        ("first_name", "last_name"),
        "date_sent",
        "status",
        "description",
    )
    list_display = (
        "check_in",
        "service",
        "date_sent",
        "pax",
        "first_name",
        "last_name",
    )
    list_filter = ("check_in", "date_sent")
    ordering = ("check_in",)
