from django.urls import path
from . import views


urlpatterns = [
    path("", views.home, name="homepage"),
    path("custom_admin_backoffice", views.login_user, name="bo_login"),
    path("logout_user", views.logout_user, name="bo_logout"),
    path("restaurant", views.restaurant, name="restaurant"),
    path("bedrooms", views.bedrooms, name="bedrooms"),
    path("activities", views.activities, name="activities"),
    path("gallery", views.gallery, name="gallery"),
    path("contact_us", views.contact_us, name="contact_us"),
    path("resa", views.resa, name="resa"),
    path("set-language-cookie/", views.set_language_cookie, name="set_language_cookie"),
    ###BO###
    ###home
    path("customadmin", views.index, name="customadmin"),
    path("slide_list", views.slide_list, name="slide_list"),
    path("slide/<slide_id>", views.update_slide, name="slide_update"),
    path("text_comp", views.update_text_comp, name="text_comp"),
    path("offer_list", views.offer_list, name="offer_list"),
    path("update_offer/<offer_id>", views.update_offer, name="update_offer"),
    path("update_todo", views.update_todo, name="update_todo"),
    path("update_todo_item/<item_id>", views.update_todo_item, name="update_todo_item"),
    ###resto
    path("update_resto_head", views.update_header_resto, name="update_resto_head"),
    path("resto_card_list", views.resto_card_list, name="resto_card_list"),
    path("update_foot_resto", views.update_footer_resto, name="update_foot_resto"),
    path("update_card1_resto", views.update_card1_resto, name="update_card1_resto"),
    path("update_card2_resto", views.update_card2_resto, name="update_card2_resto"),
    path("update_card3_resto", views.update_card3_resto, name="update_card3_resto"),
    ###bedrooms
    path("bedrooms_card_list", views.bedrooms_card_list, name="bedrooms_card_list"),
    path("update_bedcard/<bed_id>", views.update_bedcard, name="update_bedcard"),
    path(
        "update_bed_paragraphe",
        views.update_bed_paragraphe,
        name="update_bed_paragraphe",
    ),
    ###activities
    path("activity_card_list", views.activity_card_list, name="activity_card_list"),
    path("update_actcard/<card_id>", views.update_actcard, name="update_actcard"),
    path(
        "update_act_paragraphe",
        views.update_act_paragraphe,
        name="update_act_paragraphe",
    ),
    ###gallery
    path("gallery_card_list", views.gallery_card_list, name="gallery_card_list"),
    path(
        "update_galley_card/<card_id>",
        views.update_galley_card,
        name="update_galley_card",
    ),
    ###Contacts
    path(
        "update_Contacts_paragraphe",
        views.update_Contacts_paragraphe,
        name="update_Contacts_paragraphe",
    ),
    path("update_localisation", views.update_loc, name="update_loc"),
    path("update_contacts", views.update_con, name="update_con"),
    ###Footers
    path("update_Footer", views.update_Footer, name="update_Footer"),
    ###Resa
    path("resa_inbox", views.resa_inbox, name="resa_inbox"),
    path("view_resa/<card_id>", views.view_resa, name="view_resa"),
    path("delete_resa/<resa_id>", views.delete_resa, name="delete-resa"),
    ###Message
    path("mess_inbox", views.mess_inbox, name="mess_inbox"),
    path("view_mess/<card_id>", views.view_mess, name="view_mess"),
    path("delete_mess/<mess_id>", views.delete_mess, name="delete-mess"),
]
