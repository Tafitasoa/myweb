from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from datetime import datetime
from .models import Resa
from .models import MessageContact
from .forms import ResaForm
from django.core.mail import send_mail
from .models import (
    SlideHome,
    ComponentHome,
    OfferHome,
    ItemTodoHome,
    ParagrapheTodoHome,
    OfferHomeDetails,
)
from .models import Footer, Contacts, Navbar
from .models import (
    ParagrapheAnimeResto,
    ParagrapheFootResto,
    Card1Resto,
    Card2Resto,
    Card3Resto,
)
from .models import CardBedrooms, ParagrapheBedrooms
from .models import CardActivities, ParagrapheActivities
from .models import GalleryGrid
from .models import LocalisationVohitsoa

from .forms import (
    SlideForm,
    ComponentHomeForm,
    OfferHomeForm,
    ItemTodoHomeForm,
    ParagrapheTodoHomeForm,
    OfferHomeDetailsForm,
)
from .forms import FooterForm, ContactsForm
from .forms import (
    ParagrapheAnimeRestoForm,
    ParagrapheFootRestoForm,
    Card1RestoForm,
    Card2RestoForm,
    Card3RestoForm,
)
from .forms import CardBedroomsForm, Paragraphe_bedrooms_Form
from .forms import CardActivitiesForm, Paragraphe_activities_Form
from .forms import GalleryGridForm
from .forms import LocalisationVohitsoaForm

from django.utils.translation import gettext as _
from django.http import JsonResponse

from django.utils import timezone
from django.core.exceptions import ValidationError

from django.core.paginator import Paginator


def set_language_cookie(request):
    language_code = request.GET.get("language_code")
    response = JsonResponse({"success": True})
    if language_code:
        response.set_cookie("django_language", language_code)
    return response


def resa(request):
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year
    chaine = _("Check-out date must be after check-in date.")
    if request.method == "POST":
        form = ResaForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            check_in = data.get("check_in")
            check_out = data.get("check_out")
            # Perform the validation on the check-in date and check-out date (if provided)
            if check_in < timezone.now().date() or check_in > check_out:
                form.add_error(
                    "check_in",
                    ValidationError(_("Check-in date must be equal or after today.")),
                )
                messageresa_fail = f"Booking NOT sent: Please verify your check_in date"
                messageresa_failfr = f"La reservation n'est PAS envoyée: veuiller verifier la date du check_in "
                return render(
                    request,
                    "website/home.html",
                    {
                        "form": form,
                        "footers": footers,
                        "contacts": contacts,
                        "navbar": navbar,
                        "messageresa_fail": messageresa_fail,
                        "messageresa_failfr": messageresa_failfr,
                        "language": language,
                        "current_year": current_year,
                        "chaine": chaine,
                    },
                )
            # If the form is valid, save the reservation and send the email
            if not form.errors:
                resa = form.save(commit=False)
                resa.date_sent = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
                resa.status = "pending"
                resa.save()

                message_content = (
                    f"Check-in: {data['check_in']}\n"
                    f"Check-out: {data['check_out']}\n"
                    f"Service: {data['service']}\n"
                    f"Pax number: {data['pax']}\n"
                    f"First name: {data['first_name']}\n"
                    f"Last name: {data['last_name']}\n"
                    f"Phone number: {data['phone']}\n"
                    f"Email address: {data['email_address']}\n"
                    f"Description: {data['description']}"
                )
                send_mail(
                    f"Reservation from website by {data['first_name']} {data['last_name']}",
                    message_content,
                    data["email_address"],
                    ["andrintsu@gmail.com"],
                    fail_silently=False,
                )

                messageresa_successa = f"Thank you ❤ {data['first_name']}  {data['last_name']} we have received your BOOKING"
                messageresa_successf = f"{data['first_name']}  {data['last_name']}, nous avons reçu votre réservation, L'équipe Vohitsoa vous remercie ❤"
                return render(
                    request,
                    "website/home.html",
                    {
                        "form": form,
                        "footers": footers,
                        "contacts": contacts,
                        "navbar": navbar,
                        "messageresa_successa": messageresa_successa,
                        "messageresa_successf": messageresa_successf,
                        "language": language,
                        "current_year": current_year,
                    },
                )
    else:
        form = ResaForm()

    return render(
        request,
        "website/home.html",
        {
            "form": form,
            "language": language,
            "current_year": current_year,
        },
    )


def contact_us(request):
    form = ResaForm
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    positions = LocalisationVohitsoa.objects.all()
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year

    if request.method == "POST":
        message_name = request.POST["message_name"]
        message_email = request.POST["message_email"]
        message = request.POST["message"]

        if message_name and message_email:
            send_mail(
                f"Message from website by {message_name}",
                (
                    f"De la part de: {message_name}\nAdresse email: {message_email}\nmessage:\n"
                    + message
                ),
                message_email,
                ["andrintsu@gmail.com"],  # can add a coma then add a second email
                fail_silently=False,
            )

            # Save message to database
            message_contact = MessageContact(
                message_name=message_name,
                message_email=message_email,
                message_content=message,
                date_sent=timezone.now().strftime("%Y-%m-%d %H:%M:%S"),
                status="pending",
            )
            message_contact.save()

            messageme_successa = (
                f"Thank you {message_name} we have received your message"
            )
            messageme_successf = f"{message_name}, nous avons reçu votre message, L'équipe Vohitsoa vous remercie"
            return render(
                request,
                "website/contact_us.html",
                {
                    "form": form,
                    "messageme_successa": messageme_successa,
                    "messageme_successf": messageme_successf,
                    "footers": footers,
                    "navbar": navbar,
                    "language": language,
                    "current_year": current_year,
                    "contacts": contacts,
                },
            )
        else:
            messageme_successa = f"Please fill up all the fields"
            messageme_successf = f"Veuiller completer tous les champs s'il vous plaît"
            return render(
                request,
                "website/contact_us.html",
                {
                    "form": form,
                    "messageme_successa": messageme_successa,
                    "messageme_successf": messageme_successf,
                    "footers": footers,
                    "navbar": navbar,
                    "language": language,
                    "current_year": current_year,
                    "contacts": contacts,
                },
            )
    else:
        return render(
            request,
            "website/contact_us.html",
            {
                "form": form,
                "footers": footers,
                "contacts": contacts,
                "navbar": navbar,
                "language": language,
                "positions": positions,
                "current_year": current_year,
            },
        )


def gallery(request):
    form = ResaForm
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    galleries = GalleryGrid.objects.all()
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year

    return render(
        request,
        "website/gallery.html",
        {
            "form": form,
            "footers": footers,
            "contacts": contacts,
            "navbar": navbar,
            "language": language,
            "current_year": current_year,
            "galleries": galleries,
        },
    )


def activities(request):
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    actcards = CardActivities.objects.all()
    paragraphes = ParagrapheActivities.objects.all()
    form = ResaForm
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year

    return render(
        request,
        "website/activities.html",
        {
            "form": form,
            "footers": footers,
            "contacts": contacts,
            "navbar": navbar,
            "language": language,
            "current_year": current_year,
            "actcards": actcards,
            "paragraphes": paragraphes,
        },
    )


def bedrooms(request):
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    bedcards = CardBedrooms.objects.all()
    paragraphes = ParagrapheBedrooms.objects.all()
    form = ResaForm
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year

    return render(
        request,
        "website/bedrooms.html",
        {
            "form": form,
            "footers": footers,
            "contacts": contacts,
            "navbar": navbar,
            "bedcards": bedcards,
            "language": language,
            "current_year": current_year,
            "paragraphes": paragraphes,
        },
    )


def restaurant(request):
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    form = ResaForm
    heads = ParagrapheAnimeResto.objects.all()
    foots = ParagrapheFootResto.objects.all()
    card1s = Card1Resto.objects.all()
    card2s = Card2Resto.objects.all()
    card3s = Card3Resto.objects.all()
    language = request.LANGUAGE_CODE
    current_year = datetime.now().year

    return render(
        request,
        "website/restaurant.html",
        {
            "form": form,
            "heads": heads,
            "foots": foots,
            "footers": footers,
            "contacts": contacts,
            "navbar": navbar,
            "language": language,
            "current_year": current_year,
            "card1s": card1s,
            "card2s": card2s,
            "card3s": card3s,
        },
    )


def home(request):
    footers = Footer.objects.all()
    navbar = Navbar.objects.all()
    contacts = Contacts.objects.all()
    parag = ComponentHome.objects.all()
    slides = SlideHome.objects.all()
    offers = OfferHome.objects.all()
    detailsss = OfferHomeDetails.objects.all()
    todo_text = ParagrapheTodoHome.objects.all()
    todo_item = ItemTodoHome.objects.all()
    current_year = datetime.now().year
    form = ResaForm
    language = request.LANGUAGE_CODE

    return render(
        request,
        "website/home.html",
        {
            "form": form,
            "slides": slides,
            "parag": parag,
            "offers": offers,
            "detailsss": detailsss,
            "todo_text": todo_text,
            "todo_item": todo_item,
            "footers": footers,
            "contacts": contacts,
            "navbar": navbar,
            "language": language,
            "current_year": current_year,
        },
    )


#############################################################
# 															#
# 		Back-Office											#
# 															#
# #########################################################	#


#############################################################
# 															#
# 		Home												#
# 															#
# #########################################################	#


def update_todo_item(request, item_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    todo_item = ItemTodoHome.objects.get(pk=item_id)
    form = ItemTodoHomeForm(
        request.POST or None, request.FILES or None, instance=todo_item
    )

    if form.is_valid():
        form.save()
        return redirect("update_todo")

    return render(
        request,
        "website/xbo_todo_item_update.html",
        {
            "todo_item": todo_item,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_todo(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    todo_items = ItemTodoHome.objects.all()
    detail = OfferHomeDetails.objects.get(pk=1)
    todo_text = ParagrapheTodoHome.objects.get(pk=1)
    form1 = OfferHomeDetailsForm(
        request.POST or None, request.FILES or None, instance=detail
    )
    form2 = ParagrapheTodoHomeForm(
        request.POST or None, request.FILES or None, instance=todo_text
    )

    if form1.is_valid() and form2.is_valid():
        form1.save()
        form2.save()
        return redirect("update_todo")

    return render(
        request,
        "website/xbo_todo_update.html",
        {
            "todo_items": todo_items,
            "form1": form1,
            "form2": form2,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_offer(request, offer_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    offer = OfferHome.objects.get(pk=offer_id)
    form = OfferHomeForm(request.POST or None, request.FILES or None, instance=offer)
    if form.is_valid():
        form.save()
        return redirect("offer_list")
    return render(
        request,
        "website/xbo_offer_update.html",
        {
            "offer": offer,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def offer_list(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    offers = OfferHome.objects.all()
    return render(
        request,
        "website/xbo_offer_list.html",
        {
            "offers": offers,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_text_comp(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    parag = ComponentHome.objects.get(pk=1)
    form = ComponentHomeForm(
        request.POST or None, request.FILES or None, instance=parag
    )
    if form.is_valid():
        form.save()
        return redirect("text_comp")
    return render(
        request,
        "website/xbo_text_component.html",
        {
            "parag": parag,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_slide(request, slide_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    slide = SlideHome.objects.get(pk=slide_id)
    form = SlideForm(request.POST or None, request.FILES or None, instance=slide)
    if form.is_valid():
        form.save()
        return redirect("slide_list")
    return render(
        request,
        "website/xbo_slide_update.html",
        {
            "slide": slide,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def slide_list(request):
    current_year = datetime.now().year
    slides = SlideHome.objects.all()
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    return render(
        request,
        "website/xbo_slide.html",
        {
            "current_year": current_year,
            "slides": slides,
            "pendig_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def index(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    return render(
        request,
        "website/home_backend.html",
        {
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Restaurant											#
    # 															#
    # #########################################################	#


def update_card3_resto(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    c3 = Card3Resto.objects.get(pk=1)
    form = Card3RestoForm(request.POST or None, request.FILES or None, instance=c3)

    if form.is_valid():
        form.save()
        return redirect("resto_card_list")

    return render(
        request,
        "website/xbo_resto_card3_update.html",
        {
            "c3": c3,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_card2_resto(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    c2 = Card2Resto.objects.get(pk=1)
    form = Card2RestoForm(request.POST or None, request.FILES or None, instance=c2)

    if form.is_valid():
        form.save()
        return redirect("resto_card_list")

    return render(
        request,
        "website/xbo_resto_card2_update.html",
        {
            "c2": c2,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_card1_resto(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    c1 = Card1Resto.objects.get(pk=1)
    form = Card1RestoForm(request.POST or None, request.FILES or None, instance=c1)

    if form.is_valid():
        form.save()
        return redirect("resto_card_list")

    return render(
        request,
        "website/xbo_resto_card1_update.html",
        {
            "c1": c1,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_footer_resto(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    foot = ParagrapheFootResto.objects.get(pk=1)
    form = ParagrapheFootRestoForm(
        request.POST or None, request.FILES or None, instance=foot
    )

    if form.is_valid():
        form.save()
        return redirect("update_foot_resto")

    return render(
        request,
        "website/xbo_resto_foot_update.html",
        {
            "head": foot,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_header_resto(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    head = ParagrapheAnimeResto.objects.get(pk=1)
    form = ParagrapheAnimeRestoForm(
        request.POST or None, request.FILES or None, instance=head
    )

    if form.is_valid():
        form.save()
        return redirect("update_resto_head")

    return render(
        request,
        "website/xbo_resto_head_update.html",
        {
            "head": head,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def resto_card_list(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    c1 = Card1Resto.objects.all()
    c2 = Card2Resto.objects.all()
    c3 = Card3Resto.objects.all()

    return render(
        request,
        "website/xbo_restocards_list.html",
        {
            "c1": c1,
            "c2": c2,
            "c3": c3,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Bedrooms											#
    # 															#
    # #########################################################	#


def bedrooms_card_list(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    b = CardBedrooms.objects.all()

    return render(
        request,
        "website/xbo_bedcards_list.html",
        {
            "c1": b,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_bedcard(request, bed_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    b = CardBedrooms.objects.get(pk=bed_id)
    form = CardBedroomsForm(request.POST or None, request.FILES or None, instance=b)

    if form.is_valid():
        form.save()
        return redirect("bedrooms_card_list")

    return render(
        request,
        "website/xbo_bedcard_update.html",
        {
            "b": b,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_bed_paragraphe(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    p = ParagrapheBedrooms.objects.get(pk=1)
    form = Paragraphe_bedrooms_Form(
        request.POST or None, request.FILES or None, instance=p
    )

    if form.is_valid():
        form.save()
        return redirect("update_bed_paragraphe")

    return render(
        request,
        "website/xbo_update_bed_paragraphe.html",
        {
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Activities											#
    # 															#
    # #########################################################	#


def activity_card_list(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    ca = CardActivities.objects.all()

    return render(
        request,
        "website/xbo_actcards_list.html",
        {
            "ca": ca,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_actcard(request, card_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    ca = CardActivities.objects.get(pk=card_id)
    form = CardActivitiesForm(request.POST or None, request.FILES or None, instance=ca)

    if form.is_valid():
        form.save()
        return redirect("activity_card_list")

    return render(
        request,
        "website/xbo_actcard_update.html",
        {
            "b": ca,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_act_paragraphe(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    p = ParagrapheActivities.objects.get(pk=1)
    form = Paragraphe_activities_Form(
        request.POST or None, request.FILES or None, instance=p
    )

    if form.is_valid():
        form.save()
        return redirect("update_act_paragraphe")

    return render(
        request,
        "website/xbo_update_act_paragraphe.html",
        {
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Gallery												#
    # 															#
    # #########################################################	#


def gallery_card_list(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    ga = GalleryGrid.objects.all()

    return render(
        request,
        "website/xbo_gallery_list.html",
        {
            "ga": ga,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_galley_card(request, card_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    ga = GalleryGrid.objects.get(pk=card_id)
    form = GalleryGridForm(request.POST or None, request.FILES or None, instance=ga)

    if form.is_valid():
        form.save()
        return redirect("gallery_card_list")

    return render(
        request,
        "website/xbo_gallery_card_update.html",
        {
            "b": ga,
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Contacts											#
    # 															#
    # #########################################################	#


def update_Contacts_paragraphe(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    l = LocalisationVohitsoa.objects.get(pk=1)
    co = Contacts.objects.get(pk=1)

    form1 = LocalisationVohitsoaForm(
        request.POST or None, request.FILES or None, instance=l
    )
    form2 = ContactsForm(request.POST or None, request.FILES or None, instance=co)

    return render(
        request,
        "website/xbo_contacts_update.html",
        {
            "form1": form1,
            "form2": form2,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def update_loc(request):
    l = LocalisationVohitsoa.objects.get(pk=1)
    form1 = LocalisationVohitsoaForm(
        request.POST or None, request.FILES or None, instance=l
    )
    if form1.is_valid():
        form1.save()
        return redirect("update_Contacts_paragraphe")


def update_con(request):
    co = Contacts.objects.get(pk=1)
    form2 = ContactsForm(request.POST or None, request.FILES or None, instance=co)
    if form2.is_valid():
        form2.save()
        return redirect("update_Contacts_paragraphe")

        #############################################################
        # 															#
        # 		Footer												#
        # 															#
        # #########################################################	#


def update_Footer(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    f = Footer.objects.get(pk=1)
    form = FooterForm(request.POST or None, request.FILES or None, instance=f)

    if form.is_valid():
        form.save()
        return redirect("update_Footer")

    return render(
        request,
        "website/xbo_update_Footer.html",
        {
            "form": form,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )

    #############################################################
    # 															#
    # 		Resa												#
    # 															#
    # #########################################################	#


def resa_inbox(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    resas = Resa.objects.all().order_by("-date_sent")
    p = Paginator(resas, 10)
    page = request.GET.get("page")
    inb = p.get_page(page)
    nums = "a" * inb.paginator.num_pages

    return render(
        request,
        "website/xbo_resa_inbox.html",
        {
            "resas": resas,
            "nums": nums,
            "inb": inb,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def view_resa(request, card_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    resa = Resa.objects.get(pk=card_id)
    resa.status = "read"
    resa.save()

    return render(
        request,
        "website/xbo_view_resa.html",
        {
            "resa": resa,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def delete_resa(request, resa_id):
    resa = Resa.objects.get(pk=resa_id)
    resa.delete()
    return redirect("resa_inbox")

    #############################################################
    # 															#
    # 		Messages											#
    # 															#
    # #########################################################	#


def mess_inbox(request):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    mess = MessageContact.objects.all().order_by("-date_sent")

    p = Paginator(mess, 10)
    page = request.GET.get("page")
    inb = p.get_page(page)
    nums = "a" * inb.paginator.num_pages

    return render(
        request,
        "website/xbo_message_inbox.html",
        {
            "mess": mess,
            "nums": nums,
            "inb": inb,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def view_mess(request, card_id):
    current_year = datetime.now().year
    pending_resa = Resa.objects.filter(status="pending").count()
    pending_message = MessageContact.objects.filter(status="pending").count()
    mess = MessageContact.objects.get(pk=card_id)
    mess.status = "read"
    mess.save()

    return render(
        request,
        "website/xbo_view_message.html",
        {
            "mess": mess,
            "current_year": current_year,
            "pending_resa": pending_resa,
            "pending_message": pending_message,
        },
    )


def delete_mess(request, mess_id):
    mesa = MessageContact.objects.get(pk=mess_id)
    mesa.delete()
    return redirect("mess_inbox")


#####################################
# 									#
# 		login					#
# 									#
#####################################


def login_user(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("customadmin")
        else:
            messages.success(request, ("An error occured"))
            return redirect("bo_login")
    else:
        return render(request, "website/login.html", {})


def logout_user(request):
    logout(request)
    messages.success(request, ("You Were Logged Out!"))
    return redirect("bo_login")
