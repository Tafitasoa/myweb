# Generated by Django 4.1.7 on 2023-03-06 07:26

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("website", "0002_alter_slide_home_slide_image1_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="resa",
            name="check_in",
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name="resa",
            name="check_out",
            field=models.DateField(blank=True, null=True),
        ),
    ]
