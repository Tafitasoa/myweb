# Generated by Django 4.1.7 on 2023-03-07 06:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("website", "0004_message_contact_date_sent_resa_date_sent"),
    ]

    operations = [
        migrations.AddField(
            model_name="message_contact",
            name="status",
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name="resa",
            name="status",
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
