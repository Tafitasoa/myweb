from django import forms
from django.forms import ModelForm

from .models import Resa

from .models import (
    SlideHome,
    ComponentHome,
    OfferHome,
    OfferHomeDetails,
    ParagrapheTodoHome,
    ItemTodoHome,
)
from .models import Footer, Contacts
from .models import (
    ParagrapheAnimeResto,
    Card1Resto,
    Card2Resto,
    Card3Resto,
    ParagrapheFootResto,
)
from .models import CardBedrooms, ParagrapheBedrooms
from .models import ParagrapheActivities, CardActivities
from .models import GalleryGrid, LocalisationVohitsoa


class SlideForm(ModelForm):
    class Meta:
        model = SlideHome
        fields = (
            "slide_image1",
            "slide_image2",
            "slide_image3",
            "slide_image4",
            "slide_image5",
            "slide_image6",
            "slide_image7",
            "slide_image8",
            "slide_image9",
            "slide_image10",
        )
        labels = {
            "slide_image1": "image1\n",
            "slide_image2": "image2\n",
            "slide_image3": "image3\n",
            "slide_image4": "image4\n",
            "slide_image5": "image5\n",
            "slide_image6": "image6\n",
            "slide_image7": "image7\n",
            "slide_image8": "image8\n",
            "slide_image9": "image9\n",
            "slide_image10": "image10\n",
        }
        widgets = {}


class ComponentHomeForm(ModelForm):
    class Meta:
        model = ComponentHome
        fields = (
            "title",
            "translated_title",
            "text_home1",
            "translated_text_home1",
            "text_home2",
            "translated_text_home2",
            "text_home3",
            "translated_text_home3",
            "logo",
            "component_image",
        )
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Translated title"}
            ),
            "text_home1": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "text_home1"}
            ),
            "translated_text_home1": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_text_home1"}
            ),
            "text_home2": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "text_home2"}
            ),
            "translated_text_home2": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_text_home2"}
            ),
            "text_home3": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "text_home3"}
            ),
            "translated_text_home3": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_text_home3"}
            ),
        }


class OfferHomeForm(ModelForm):
    class Meta:
        model = OfferHome
        fields = (
            "offer_title",
            "translated_offer_title",
            "offer_details",
            "translated_offer_details",
            "offer_image",
        )

        widgets = {
            "offer_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "offer_Title"}
            ),
            "translated_offer_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_offer_Title"}
            ),
            "offer_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "offer_details"}
            ),
            "translated_offer_details": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_offer_details",
                }
            ),
        }


class OfferHomeDetailsForm(ModelForm):
    class Meta:
        model = OfferHomeDetails
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "footer_text1": "",
            "translated_footer_text1": "",
            "footer_text2": "",
            "translated_footer_text2": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_Title"}
            ),
            "footer_text1": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "footer_text1"}
            ),
            "translated_footer_text1": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_footer_text1",
                }
            ),
            "footer_text2": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "footer_text2"}
            ),
            "translated_footer_text2": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_footer_text2",
                }
            ),
        }


class ParagrapheTodoHomeForm(ModelForm):
    class Meta:
        model = ParagrapheTodoHome
        fields = "__all__"
        labels = {
            "todo_title": "",
            "translated_todo_title": "",
            "todo": "",
            "translated_todo": "",
        }
        widgets = {
            "todo_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Todo_Title"}
            ),
            "translated_todo_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_Todo_Title"}
            ),
            "todo": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Todo"}
            ),
            "translated_todo": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Todo"}
            ),
        }


class ItemTodoHomeForm(ModelForm):
    class Meta:
        model = ItemTodoHome
        fields = "__all__"
        widgets = {
            "item_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "item_details"}
            ),
            "translated_item_details": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_item_details",
                }
            ),
        }


class FooterForm(ModelForm):
    class Meta:
        model = Footer
        fields = "__all__"
        labels = {
            "big_text": "",
            "translated_big_text": "",
            "big_text2": "",
            "translated_big_text2": "",
            "logo": "",
            "small_text": "",
            "translated_small_text": "",
        }
        widgets = {
            "big_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Text1"}
            ),
            "translated_big_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Text1"}
            ),
            "big_text2": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Text2"}
            ),
            "translated_big_text2": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Text2"}
            ),
            "small_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Text3"}
            ),
            "translated_small_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Text3"}
            ),
        }


class ContactsForm(ModelForm):
    class Meta:
        model = Contacts
        fields = "__all__"
        widgets = {
            "email_address": forms.EmailInput(
                attrs={"class": "form-control", "placeholder": "Email address"}
            ),
            "addresse": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "addresse"}
            ),
            "phone_number1": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "phone_number1"}
            ),
            "phone_number2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "phone_number2"}
            ),
            "phone_number3": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "phone_number3"}
            ),
            "facebook_link": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "facebook_link"}
            ),
            "instagram_link": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "instagram_link"}
            ),
        }


class ParagrapheAnimeRestoForm(ModelForm):
    class Meta:
        model = ParagrapheAnimeResto
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "big_text": "",
            "translated_big_text": "",
            "small_text": "",
            "translated_small_text": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "big_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "big_text"}
            ),
            "translated_big_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_big_text"}
            ),
            "small_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "small_text"}
            ),
            "translated_small_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_small_text"}
            ),
        }


class Card1RestoForm(ModelForm):
    class Meta:
        model = Card1Resto
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "details": "",
            "translated_details": "",
            "paragraphe_title": "",
            "translated_paragraphe_title": "",
            "paragraphe_text": "",
            "translated_paragraphe_text": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "details"}
            ),
            "translated_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_details"}
            ),
            "paragraphe_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "paragraphe_title"}
            ),
            "translated_paragraphe_title": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_title",
                }
            ),
            "paragraphe_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "paragraphe_text"}
            ),
            "translated_paragraphe_text": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_text",
                }
            ),
        }


class Card2RestoForm(ModelForm):
    class Meta:
        model = Card2Resto
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "details": "",
            "translated_details": "",
            "paragraphe_title": "",
            "translated_paragraphe_title": "",
            "paragraphe_text": "",
            "translated_paragraphe_text": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "details"}
            ),
            "translated_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_details"}
            ),
            "paragraphe_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "paragraphe_title"}
            ),
            "translated_paragraphe_title": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_title",
                }
            ),
            "paragraphe_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "paragraphe_text"}
            ),
            "translated_paragraphe_text": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_text",
                }
            ),
        }


class Card3RestoForm(ModelForm):
    class Meta:
        model = Card3Resto
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "details": "",
            "translated_details": "",
            "paragraphe_title": "",
            "translated_paragraphe_title": "",
            "paragraphe_text": "",
            "translated_paragraphe_text": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "details"}
            ),
            "translated_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_details"}
            ),
            "paragraphe_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "paragraphe_title"}
            ),
            "translated_paragraphe_title": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_title",
                }
            ),
            "paragraphe_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "paragraphe_text"}
            ),
            "translated_paragraphe_text": forms.Textarea(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated_paragraphe_text",
                }
            ),
        }


class ParagrapheFootRestoForm(ModelForm):
    class Meta:
        model = ParagrapheFootResto
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "text": "",
            "translated_text": "",
            "boutton": "",
            "translated_boutton": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "text"}
            ),
            "translated_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_text"}
            ),
            "boutton": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "button label"}
            ),
            "translated_boutton": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated button label",
                }
            ),
        }


class CardBedroomsForm(ModelForm):
    class Meta:
        model = CardBedrooms
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "details": "",
            "translated_details": "",
            "price": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "details"}
            ),
            "translated_details": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_details"}
            ),
            "price": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Price"}
            ),
        }


class Paragraphe_bedrooms_Form(ModelForm):
    class Meta:
        model = ParagrapheBedrooms
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "title2": "",
            "translated_title2": "",
            "text": "",
            "translated_text": "",
            "boutton": "",
            "translated_boutton": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "title2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title2"}
            ),
            "translated_title2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title2"}
            ),
            "text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Text"}
            ),
            "translated_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Text"}
            ),
            "boutton": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Button_text"}
            ),
            "translated_boutton": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_Button_text"}
            ),
        }


class Paragraphe_activities_Form(ModelForm):
    class Meta:
        model = ParagrapheActivities
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "text": "",
            "translated_text": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "Text"}
            ),
            "translated_text": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "translated_Text"}
            ),
        }


class CardActivitiesForm(ModelForm):
    class Meta:
        model = CardActivities
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
        }


class GalleryGridForm(ModelForm):
    class Meta:
        model = GalleryGrid
        fields = "__all__"
        labels = {
            "title": "",
            "translated_title": "",
            "image1": "",
            "image2": "",
            "image3": "",
            "image4": "",
            "image5": "",
            "image6": "",
            "image7": "",
            "image8": "",
            "image9": "",
            "image10": "",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
        }


class LocalisationVohitsoaForm(ModelForm):
    class Meta:
        model = LocalisationVohitsoa
        fields = "__all__"
        labels = {
            "url": "URL",
            "placeh1": "placeholder nom",
            "translated_placeh1": "Translated placeholder nom",
            "placeh2": "placeholder email",
            "translated_placeh2": "Translated placeholder email",
            "placeh3": "placeholder message",
            "translated_placeh3": "Translated placeholder message",
            "boutton": "Boutton envoyer text",
            "translated_boutton": "Translated Boutton envoyer text",
        }
        widgets = {
            "title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "translated_title": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title"}
            ),
            "url": forms.TextInput(attrs={"class": "form-control"}),
            "title2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "title2"}
            ),
            "translated_title2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "translated_title2"}
            ),
            "placeh1": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "placeholder 1"}
            ),
            "translated_placeh1": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated placeholder 1",
                }
            ),
            "placeh2": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "placeholder 2"}
            ),
            "translated_placeh2": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated placeholder 2",
                }
            ),
            "placeh3": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "placeholder 3"}
            ),
            "translated_placeh3": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated placeholder 3",
                }
            ),
            "boutton": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Button label"}
            ),
            "translated_boutton": forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "translated Button label",
                }
            ),
        }


class DateInput(forms.DateInput):
    input_type = "date"


class ResaForm(ModelForm):
    class Meta:
        model = Resa
        fields = (
            "check_in",
            "check_out",
            "service",
            "pax",
            "first_name",
            "last_name",
            "phone",
            "email_address",
            "description",
        )
        labels = {
            "check_in": "Check-in",
            "check_out": "Check-out (optional)",
            "service": "Service",
            "pax": "Pax number",
            "first_name": "",
            "last_name": "",
            "phone": "",
            "email_address": "",
            "description": "",
        }
        widgets = {
            "check_in": DateInput(attrs={"class": "form-control", "placeholder": ""}),
            "check_out": DateInput(attrs={"class": "form-control", "placeholder": ""}),
            "service": forms.Select(
                attrs={"class": "form-control", "placeholder": "Service"}
            ),
            "pax": forms.NumberInput(
                attrs={"class": "form-control", "placeholder": "1"}
            ),
            "first_name": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "First name"}
            ),
            "last_name": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Last name"}
            ),
            "phone": forms.TextInput(
                attrs={"class": "form-control", "placeholder": "Phone number"}
            ),
            "email_address": forms.EmailInput(
                attrs={"class": "form-control", "placeholder": "Email address"}
            ),
            "description": forms.Textarea(
                attrs={"class": "form-control", "placeholder": "More details"}
            ),
        }
