from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("whothehellareyouadmin/", admin.site.urls),
    path("", include("website.urls")),
    path("members/", include("django.contrib.auth.urls")),
    path("members/", include("members.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# Configure admin titles
admin.site.site_header = "Admin vohitsoa"
admin.site.site_title = "Admin vohitsoa"
admin.site.index_title = ""
