from django.urls import path
from . import views

urlpatterns = [
    path("custom_administration_backoffice", views.login_user, name="login"),
    path("logout_user", views.logout_user, name="logout"),
]
